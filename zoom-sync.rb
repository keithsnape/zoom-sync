# This script downloads all the recordings from a Zoom account and
# uploads them to a directory on Google Drive. The first attempt
# to upload to Google Drive will attempt to authenticate and
# create a local config.json with the credentials.
#
# +# Also prerequisite: apt-get install mediainfo (or apk add mediainfo)
require 'date'
require 'json'
require 'pp'
require 'net/http'
require 'open-uri'
require 'google_drive'
require 'yaml'
require 'jwt'
require 'ostruct'
require 'httparty'
require 'mediainfo'

# Client to access Zoom.us recordings and paginate records
class ZoomClient
  include HTTParty
  MAX_PAGE_SIZE = 300
  RECORDINGS_URL = "https://api.zoom.us/v2/users/USER_ID/recordings".freeze
  USERS_URL = 'https://api.zoom.us/v2/users'.freeze

  attr_reader :api_key, :api_secret

  headers 'Accept' => 'application/json'
  headers 'Content-Type' => 'application/json'

  def initialize(config)
    @api_key = config['api_key']
    @api_secret = config['api_secret']
  end

  def users
    paginated_get(USERS_URL, 'users')
  end

  # Since we no longer delete recordings, we need to time-bound the
  # recording date to avoid having to process an inordinate number of
  # recordings.
  def recordings(user_id:, from: Date.today.prev_day, to: Date.today)
    paginated_get(RECORDINGS_URL.sub('USER_ID', user_id),
                  'meetings',
                  { 'from' => from.to_s, 'to' => to.to_s })
  end

  def download_file(url, filename)
    bytes_written = 0
    File.open(filename, 'w') do |file|
      file.binmode
      self.class.get(url, follow_redirects: true) do |fragment|
        bytes_written += file.write(fragment)
      end
    end

    bytes_written
  end

  private

  def access_token
    JWT.encode({ iss: api_key, exp: Time.now.to_i + 86400 }, api_secret, 'HS256', { typ: 'JWT' })
  end

  def request_headers
    {
      'Accept' => 'application/json',
      'Content-Type' => 'application/json',
      'Authorization' => "Bearer #{access_token}"
    }
  end

  def paginated_get(url, item_name, options = {})
    options['page_size'] = MAX_PAGE_SIZE
    page_number = 1

    Enumerator.new do |block|
      loop do
        options['page_number'] = page_number
        res = self.class.get(url, query: options, headers: request_headers)

        if res.code != 200
          puts "Error retrieving #{url}: #{res.code}"
          exit
        end

        body = JSON.parse(res.body)

        total_pages = body['page_count']

        break unless total_pages

        body.fetch(item_name, {}).each do |element|
          block.yield element
        end

        break if page_number >= total_pages
        page_number += 1
      end
    end
  end
end

# Client to access Google Drive
class GoogleDriveClient
  GOOGLE_BASE_FOLDER = 'GitLab Videos'.freeze

  attr_reader :session

  def initialize(config)
    @session = GoogleDrive::Session.from_config(config)
  end

  def valid_file?(folders, filename, file_size)
    dest_folder = session.file_by_title([GOOGLE_BASE_FOLDER] + folders)

    return unless dest_folder

    dest_file = dest_folder.file_by_title(filename)
    puts "Checking for existence #{filename}..."

    return false unless dest_file

    dest_file_size = dest_file.size.to_i

    if dest_file_size != file_size
      puts "Google Drive has #{filename} with #{dest_file_size} bytes, expecting #{file_size}"
    end

    dest_file_size == file_size
  end

  def mkdir(sub_directories)
    current_path = [GOOGLE_BASE_FOLDER]
    dest_folder = nil

    sub_directories.each do |dir|
      current_folder = session.file_by_title(current_path)
      current_path += [dir]
      dest_folder = session.file_by_title(current_path)

      unless dest_folder
        puts "Creating destination folder #{current_path}"
        dest_folder = current_folder.create_subcollection(dir)
      end
    end

    dest_folder
  end

  def upload_video(sub_directories, filename, file_size)
    dest_folder = mkdir(sub_directories)

    puts "Uploading #{filename} to #{sub_directories}"
    dest_filename = dest_folder.file_by_title(filename)

    if dest_filename
      puts "File #{filename} already exists, removing"
      dest_filename.delete
    end

    begin
      uploaded = dest_folder.upload_from_file(filename)
    rescue Google::Apis::ServerError => e
      puts "Google API error: #{e}"
      return false
    end

    # Google Drive treats anything < 50K as 0 bytes, so we
    # have to assume it was uploaded if we got something back
    return true if uploaded && file_size < 51200

    uploaded.size.to_i == file_size
  end
end

# Iterates through all Zoom.us recordings and transfers them to a folder
# in Google Drive
class ZoomSyncher
  attr_reader :client, :gdrive_client

  MAX_FILE_SIZE_BYTES = 2000000000 # 2 GB

  def initialize
    @client = ZoomClient.new(load_zoom_config)
    @gdrive_client = GoogleDriveClient.new(load_gdrive_config)
  end

  def sync
    start_date = ENV['START_DATE'] || Date.today.prev_day
    end_date = ENV['END_DATE'] || Date.today
    # Restrict scan to certain users
    user_email = ENV['EMAIL']

    puts "Searching for recordings from #{start_date} to #{end_date}"

    client.users.each do |user|
      host_id = user['id']
      next if user_email && user['email'] != user_email

      puts "Scanning recordings for #{user['email']}"

      client.recordings(user_id: host_id, from: start_date, to: end_date).each do |recording|
        sync_recording(recording)
      end

      # Zoom rate limits API requests, so pause between users
      # to honor their limits
      sleep 1
    end
  end

  def sync_recording(item)
    topic = item['topic'].strip
    recording_files = item['recording_files']

    return unless recording_files

    puts "Found #{recording_files.count} recordings"

    # There can be multiple files with the same UUID. Include
    # the index to ensure that we download all recordings.
    recording_files.each_with_index do |file, index|
      event_date = Date.rfc3339(file['recording_start']).to_s
      uuid = item['uuid']
      file_type = file['file_type']

      puts "Found recording: #{topic} with type #{file_type}"

      unless file_type
        puts "Skipping #{topic} since no file type is available yet; recording is likely still being processed"
        next
      end

      ext = file_extension(file_type)
      folder = subfolder(topic, file_type)
      filename = sanitize_filename("#{topic}-#{event_date}-#{uuid}-#{index}") + ".#{ext}"
      video_url = file['download_url']
      file_size = file['file_size']

      if file_size.to_i > MAX_FILE_SIZE_BYTES
        puts "Skipping #{filename}, file size is #{file_size}, limit is #{MAX_FILE_SIZE_BYTES}"
      elsif gdrive_client.valid_file?(folder, filename, file_size)
        puts "Skipping #{filename}, already exists"
      else
        success = download_video(video_url, filename, file_size)

        unless success
          puts "Failed to download video #{filename}, skipping"
          next
        end

        if should_upload?(filename, file_type)
          success = gdrive_client.upload_video(folder, filename, file_size)

          puts "Failed to upload video #{filename}, skipping" unless success
        else
          puts "Skipping #{filename} because file does not have an audio track > 30 s"
        end

        File.delete(filename)
      end
    end
  end

  private

  def load_zoom_config
    if ENV['ZOOM_API_KEY'] && ENV['ZOOM_API_SECRET']
      {
        'api_key' => ENV['ZOOM_API_KEY'],
        'api_secret' => ENV['ZOOM_API_SECRET']
      }
    else
      YAML.load_file('zoom_sync.yml')
    end
  end

  # Returns a config object that responds to client_id, etc.:
  # https://www.rubydoc.info/gems/google_drive/2.1.1/GoogleDrive%2FSession.from_config
  def load_gdrive_config
    data =
      if ENV['GOOGLE_DRIVE_CONFIG']
        ENV['GOOGLE_DRIVE_CONFIG']
      else
        File.read('config.json')
      end

    JSON.parse(data, object_class: OpenStruct)
  end

  # Discard any audio or videos that aren't longer than 30 seconds
  def should_upload?(filename, file_type)
    return true if file_type.downcase == 'chat'

    duration = MediaInfo.from(filename)&.audio&.duration

    duration.to_i > 30
  end

  def download_video(url, filename, file_size)
    STDERR.puts "Downloading #{filename}"
    attempts = 0

    loop do
      begin
        bytes = client.download_file(url, filename)

        return true if bytes == file_size

        puts "Mismatch in file size: downloaded #{bytes}, expected #{file_size}, retrying..."
      rescue OpenURI::HTTPError => e
        puts "Error downloading file: #{e}, retrying..."
      end

      attempts += 1

      return false if attempts > 2
    end
  end

  def subfolder(topic, file_type)
    return [topic, 'audio'] if file_type.casecmp('m4a').zero?

    [topic]
  end

  def file_extension(file_type)
    file_type = file_type.downcase

    return 'txt' if file_type == 'chat'

    file_type
  end

  def sanitize_filename(filename)
    filename.downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '')
  end

end

if $PROGRAM_NAME == __FILE__
  syncher = ZoomSyncher.new
  syncher.sync
end
