# README

This script uploads recordings from our Zoom account and uploads them to
Google Drive. We originally did this because the Zoom storage was
limited and costly, but we continue do this to enable all team members to
view the videos.

NOTE: This script no longer purges videos from Zoom, and it does not
retain state. Because of this, the script will scan yesterday's videos and
only upload them if they do not exist on Google Drive already. If the
script does not run in a day, it will currently skip over those videos.

## Configuration

`zoom-sync.rb` can be run solely via environment variables. If the environment
variables do not exist, Zoom will attempt to load them from config files.

### Environment variables

|Variable|Description|
|--------|-----------|
|ZOOM_API_KEY|Zoom API client key|
|ZOOM_API_SECRET|Zoom API secret key|
|GOOGLE_DRIVE_CONFIG|Google Drive credentials|

If `ZOOM_API_KEY` and `ZOOM_API_SECRET` are not available, the script
will attempt to load `zoom_sync.yml`:

```yaml
---
api_key: <YOUR API KEY>
api_secret: <YOUR API SECRET>
```

If `GOOGLE_DRIVE_CONFIG` is not available, the script will attempt to
load `config.json`. If that file does not exist, the Google API will
attempt to obtain to retrieve the credentials from your existing Google
Cloud account.

```json
{
  "client_id": "YOUR CLIENT ID",
  "client_secret": "YOUR CLIENT SECRET",
  "scope": [
    "https://www.googleapis.com/auth/drive",
    "https://spreadsheets.google.com/feeds/"
  ],
  "refresh_token": "YOUR REFRESH TOKEN"
}
```

## Run-time parameters

If you need to sync a meeting that was created previous to the current
date, you can also specify a date range (up to 6 months from today):

|Variable|Description|
|--------|-----------|
|START_DATE|Starting date to search for recordings (YYYY-MM-DD)|
|END_DATE|End date to search for recordings (YYYY-MM-DD)|
|EMAIL|By default, the script will search all accounts for videos. This variable restrict scan for recordings to specific e-mail addresses.|